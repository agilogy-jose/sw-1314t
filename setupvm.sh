#!/usr/bin/env bash
JAVA_VERSION="7"
PLAY_VERSION="2.1.3"
apt-get update

#Set the environment variables in `environment` as globar env vars
while IFS='=' read NAME VALUE
do
  grep -v "${NAME}" /etc/environment > newfile; mv newfile /etc/environment
  echo "${NAME}=${VALUE}" >> /etc/environment
done < "/vagrant/environment"

#Install Java so that we can run the application 
apt-get install -y openjdk-$JAVA_VERSION-jdk
if [ ! $? -eq 0 ]
  then
    echo "Java could not be installed"
    exit -1
fi

#Install play framework 
if [ ! -e /home/vagrant/play-$PLAY_VERSION ]
  then
    echo "Installing play framework"
    apt-get install -y unzip
    cd /home/vagrant 
    wget http://downloads.typesafe.com/play/$PLAY_VERSION/play-$PLAY_VERSION.zip 
    unzip play-$PLAY_VERSION.zip
    chown -R vagrant:vagrant /home/vagrant/play-$PLAY_VERSION
    rm play-$PLAY_VERSION.zip
    echo "export PATH=/home/vagrant/play-${PLAY_VERSION}:$PATH" > .bash_profile
fi

