package helpers

import play.api.mvc.Result
import config.VoteLinkGlobal
import play.api.test.Helpers._
import play.api.test._
import play.api.libs.json.Json
import play.api.libs.json.JsValue

/*
 * This is a helper class for controller Specs that makes it easier to test them
 */
trait SpecHelper {

  def global = new VoteLinkGlobal
  
  //This saves us from typing the FakeApplication stuff at every test
  def app(block : => Unit) = {
    running(FakeApplication(withGlobal = Some(global))) {
      block
    }
  }
  
  def contentAsJson(result: Result): JsValue = {
	  Json.parse(contentAsString(result))
  }
}