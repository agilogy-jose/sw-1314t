package test

import org.specs2.mutable._
import play.api.test._
import play.api.test.Helpers._
import helpers.SpecHelper
import play.api.libs.json.Json
import config.VoteLinkGlobal
import models.AppInfo
import models.ExternalDataModel

/*
 * This class defines the specification of the Application controller
 */
class ApplicationSpec extends Specification with SpecHelper {
  
  "Application" should {
    
    "returns application information" in app {
      //we execute a request on a fake application so that we don't have to start a server
      val home = route(FakeRequest(GET, "/")).get
      
      //and check some response properties like the staus and the content
	  status(home) must equalTo(OK)
      
      contentAsJson(home) must equalTo(ExternalDataModel.toJson(global.appInfo))
    }
  }
}