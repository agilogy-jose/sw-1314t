import models.AppInfo
import config.VoteLinkGlobal

/*
 * This object holds the global configuration in a play application.
 * In order to avoid some issues with the default package we make it inherit
 * from a class in a package of our own
 */
object Global extends VoteLinkGlobal {

}