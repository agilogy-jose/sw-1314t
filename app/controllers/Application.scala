package controllers

import play.api._
import play.api.mvc._
import models.AppInfo
import play.api.libs.json.Json
import config.VoteLinkGlobal
import models.ExternalDataModel

/*
 * This is a controller. Controllers are the entry points of our applications.
 * In order to see the route each method responds to, we have to look at conf/routes
 */
class Application(appInfo: AppInfo) extends Controller {
  
  def index = Action {    
    Ok(ExternalDataModel.toJson(appInfo))
  }
  
}