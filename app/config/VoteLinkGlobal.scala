package config

import play.api.GlobalSettings
import models.AppInfo
import controllers._

/*
 * This object holds the global settings of the play application
 * 
 * In this case, we will use it as a central implementation repository so
 * that we can inject all the dependencies that we need
 */
class VoteLinkGlobal extends GlobalSettings {
  //Application objects
   val appInfo = AppInfo("0", "0.1", List("/links"))
   val applicationController = new Application(appInfo)
   
   //This method returns the injected object to the play infrastructure
   override def getControllerInstance[A](clazz : Class[A]) = {
    clazz match {
      case x if x.isAssignableFrom(classOf[Application]) => applicationController.asInstanceOf[A]
      case unk => throw new IllegalArgumentException(s"Unknown controller ${unk.getClass}")
    }
  }
}