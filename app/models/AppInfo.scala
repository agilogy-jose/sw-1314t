package models

//A case class is like a tuple. They behave as values and thus are typically used as DTOs
case class AppInfo(group:String, apiVersion:String, resources:List[String])
