package models

import play.api.libs.json.Json
import play.api.libs.json.JsValue

/*
 * This object is responsible for the serialization of the responses
 */
object ExternalDataModel {
      implicit val format = Json.format[AppInfo]

      def toJson(o: AppInfo): JsValue = {
        Json.toJson(o)
      }
      
      def toJson(s:String): JsValue = {
        Json.toJson(s)
      } 
}