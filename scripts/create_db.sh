#!/usr/bin/env bash

if [ ! $# -eq 1 ] 
  then
    echo "Usage: ./create_db.sh <database_name>"
    exit -1
fi
echo "Now we should invoke curl to use the mongolab API to create database ${1}"
