#!/usr/bin/env bash

if [ ! $# -eq 1 ] 
  then
    echo 'Usage: ./post_link.sh "<link>" '
    exit -1
fi
escaped_link=${1//\//\\\/}
sed_cmd="sed 's/LINK/${escaped_link}/g' sample_link.json"
json=$( eval $sed_cmd)
echo "Now we should invoke curl to use the mongolab API to insert this document"
echo $json
